//If player is moving set speed and change sprite
var move = keyboard_check(ord("S")) - keyboard_check(ord("A"));

if (move != 0) {
	facing = move;
	sprite_index = spr_player_walking;
	image_xscale = facing;
} else {
	sprite_index = spr_player_standing;	
}

/*Light pulse check
if (pulse == 20) {
	pulsing = 0;	
} else if (pulse == 0) {
	pulsing = 1;	
}*/

//Is player on ground
var onGround = place_meeting(x,y+1,obj_static);

//Set move speed
xSpeed = move * walkSpeed;

//Player jump check
if (keyboard_check_pressed(vk_space)) {
	if (onGround) {
		ySpeed -= jumpSpeed;	
	}
}

//Knife Throw
if (keyboard_check_pressed(ord("Z")) && canThrow) {
	var knife = instance_create_layer(x+(facing*6),y,"instances",obj_knife);
	knife.image_xscale = facing;
	knife.hspeed = 3 * facing;
	canThrow = false;
	alarm[1] = 30;
}

//Is player falling?
if (!onGround) {
	if (ySpeed > 6) {
		ySpeed = 6;	
	} else {
		ySpeed += .5;	
	}
}

/*Movable Block
if (place_meeting(x+xSpeed,y,obj_movable_block)) {
	var movable = instance_nearest(x,y,obj_movable_block);
	if (!movable.againstWall){
		xSpeed = xSpeed/2;
		movable.x += xSpeed;
	}
}*/

//Horizontal Collision with static block
if (place_meeting(x+xSpeed,y,obj_static))
{
    while(!place_meeting(x+sign(xSpeed),y,obj_static))
    {
        x += sign(xSpeed);
    }
    xSpeed = 0;
}

if (place_meeting(x,y+ySpeed,obj_static))
{
    while(!place_meeting(x,y+sign(ySpeed),obj_static))
    {
        y += sign(ySpeed);
    }
    ySpeed = 0;
}

xSpeed += pushX;
ySpeed += pushY;

//Is Player Falling?
if (ySpeed > 0) {
	sprite_index = spr_player_falling;	
}

x+=xSpeed;
y+=ySpeed;
