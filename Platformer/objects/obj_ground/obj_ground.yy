{
    "id": "b9af83bd-edcc-4d68-9330-187c45fb4fc7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ground",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "240c4d45-f043-4a75-bb3b-2cf6ad4537ea",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d31e24b0-5054-41a2-b875-3742bfac31ab",
    "visible": true
}