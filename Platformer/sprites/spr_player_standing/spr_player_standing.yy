{
    "id": "8033abab-f922-4271-871d-772078111473",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_standing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0371f045-a634-4b40-bc6b-7e98950d7405",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8033abab-f922-4271-871d-772078111473",
            "compositeImage": {
                "id": "212f3fe4-f0f2-46b1-988b-d3af19f3ed28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0371f045-a634-4b40-bc6b-7e98950d7405",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94d8ba1f-dd1b-4c7e-b833-49c96f97e655",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0371f045-a634-4b40-bc6b-7e98950d7405",
                    "LayerId": "d3e2ab6b-3491-4e55-944e-69f62892840a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d3e2ab6b-3491-4e55-944e-69f62892840a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8033abab-f922-4271-871d-772078111473",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}