{
    "id": "d1541d1c-0af4-4ae8-8ee7-6ea23b671db2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ccfc8a9-b84c-4dd5-a7f8-48dcc5ca2692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1541d1c-0af4-4ae8-8ee7-6ea23b671db2",
            "compositeImage": {
                "id": "aee4822c-a190-41ea-b948-cf146cf72e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ccfc8a9-b84c-4dd5-a7f8-48dcc5ca2692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9dfc985-37f1-4681-842d-1feb1a143e9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ccfc8a9-b84c-4dd5-a7f8-48dcc5ca2692",
                    "LayerId": "ce0f0e78-cede-4cf4-b01e-59a2b2017da2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "ce0f0e78-cede-4cf4-b01e-59a2b2017da2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1541d1c-0af4-4ae8-8ee7-6ea23b671db2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 1
}