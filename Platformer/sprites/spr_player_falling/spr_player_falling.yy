{
    "id": "8f183bb2-6e45-4b71-a1d0-d9724e1f139b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f76dc73-8950-47a8-a0c2-a94b64e96230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f183bb2-6e45-4b71-a1d0-d9724e1f139b",
            "compositeImage": {
                "id": "3dc319f9-417a-4ff1-9278-8ab17d6d58af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f76dc73-8950-47a8-a0c2-a94b64e96230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e64b8b0-ee9d-4344-be9b-195f47641327",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f76dc73-8950-47a8-a0c2-a94b64e96230",
                    "LayerId": "c1bc6ceb-f318-4936-b83f-a4a8036bbdda"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 16,
    "layers": [
        {
            "id": "c1bc6ceb-f318-4936-b83f-a4a8036bbdda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f183bb2-6e45-4b71-a1d0-d9724e1f139b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 12,
    "yorig": 8
}