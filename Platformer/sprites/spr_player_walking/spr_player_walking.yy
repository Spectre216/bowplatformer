{
    "id": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4488cb9f-d0e6-4c8c-ab7f-bd55ae051603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "652a038b-a2e3-4a7c-b205-59e0df5f2503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4488cb9f-d0e6-4c8c-ab7f-bd55ae051603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ff9caf-7532-4db8-a5c0-b13781fefd49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4488cb9f-d0e6-4c8c-ab7f-bd55ae051603",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "01bf4aaa-5989-426f-8410-28580e916c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "2bd0801c-22a4-4af0-9d52-77df8ab91355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01bf4aaa-5989-426f-8410-28580e916c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "605ce483-b1fa-4f02-9059-80a472a6c0ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01bf4aaa-5989-426f-8410-28580e916c04",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "e2de7199-e5a4-4263-bff4-4d2b82b686b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "afd261ac-5e6c-4c03-a0ff-8523debf9a7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2de7199-e5a4-4263-bff4-4d2b82b686b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1fc218d-9302-4193-be34-1abc2dc23879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2de7199-e5a4-4263-bff4-4d2b82b686b0",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "2b0b3469-421a-433f-88cd-d3811c243eb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "e4ad0ae9-e508-46ec-ab1d-7f5610b08a43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b0b3469-421a-433f-88cd-d3811c243eb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa414364-211a-4cb0-9b3f-e2453cb5d267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b0b3469-421a-433f-88cd-d3811c243eb4",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "1edf23cd-afed-40ad-a9a6-458cc348d6b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "d8da485d-7c13-4364-a7d3-f1f13b1bdbde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1edf23cd-afed-40ad-a9a6-458cc348d6b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d54cba61-7359-418c-9af0-586dadeb5347",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1edf23cd-afed-40ad-a9a6-458cc348d6b2",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "ef39e37c-9f26-4e24-b3dd-2de9e5cba3e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "c423ee10-c17c-4837-b4cf-5d66130b8f52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef39e37c-9f26-4e24-b3dd-2de9e5cba3e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4f5899-3fca-49dc-b392-e4021eac76d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef39e37c-9f26-4e24-b3dd-2de9e5cba3e9",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "fc2075f9-9a47-4504-8c2b-cabbfd44948f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "43899b5c-845e-4cfb-8b45-08caa1a88b3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2075f9-9a47-4504-8c2b-cabbfd44948f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa1b0ad2-e250-4d0d-9b3d-bda4113b6aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2075f9-9a47-4504-8c2b-cabbfd44948f",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "42feb5e8-4bf3-4749-87f0-ee635a3f3416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "d08cecd1-1978-42b5-b7d5-0e7c9ff96510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42feb5e8-4bf3-4749-87f0-ee635a3f3416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a39c847-9ab5-4ac8-8cba-b4958c155474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42feb5e8-4bf3-4749-87f0-ee635a3f3416",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "e7415dda-a958-4e90-99eb-8c47a98cf7ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "7ded9d57-26d8-4633-9fed-db20440b98e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7415dda-a958-4e90-99eb-8c47a98cf7ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98bb2bd9-e36d-49df-ae5e-b31605ee495e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7415dda-a958-4e90-99eb-8c47a98cf7ff",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "2d3e2375-6535-478a-8084-0560ee5a35ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "c7b12edc-bca8-4220-801b-b5bd5ad1747f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d3e2375-6535-478a-8084-0560ee5a35ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9da1849-9e9f-4819-8e6a-0fe373756100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d3e2375-6535-478a-8084-0560ee5a35ef",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "eb6b29e9-afbe-455a-ba5c-ded8f968dd5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "64ccd4f1-2730-4ccc-a9bc-1e940c1ca166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb6b29e9-afbe-455a-ba5c-ded8f968dd5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a30ea46-4f06-4db2-b47a-f7da61469f37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb6b29e9-afbe-455a-ba5c-ded8f968dd5c",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        },
        {
            "id": "30ac70b6-f1f7-49fd-b512-5826864a2f60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "compositeImage": {
                "id": "93db4848-ae76-4987-a9eb-f66db4b51da9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30ac70b6-f1f7-49fd-b512-5826864a2f60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "011b3c4c-73bb-40e8-9af5-3f74fca0e0c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30ac70b6-f1f7-49fd-b512-5826864a2f60",
                    "LayerId": "99095b00-91c7-47b5-a50c-d71d6ce9220c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "99095b00-91c7-47b5-a50c-d71d6ce9220c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f337640b-50d3-46ad-97ed-516dda1ffdb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}