{
    "id": "d31e24b0-5054-41a2-b875-3742bfac31ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "863695a8-a649-4a47-ace3-d91cf3d46b6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d31e24b0-5054-41a2-b875-3742bfac31ab",
            "compositeImage": {
                "id": "4515798e-fc42-4851-b286-7b5495c9ce54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "863695a8-a649-4a47-ace3-d91cf3d46b6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fabf345d-4de9-4d97-9d3a-6e0bed7543f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "863695a8-a649-4a47-ace3-d91cf3d46b6d",
                    "LayerId": "8b4ae45d-675c-4cf0-9478-d771e0351eab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8b4ae45d-675c-4cf0-9478-d771e0351eab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d31e24b0-5054-41a2-b875-3742bfac31ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}